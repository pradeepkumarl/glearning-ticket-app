package com.glearning.ticket.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.glearning.ticket.model.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long>{

	@Query("SELECT p from Post p WHERE " +
	            " p.title LIKE CONCAT('%', :query, '%') OR " +
	            " p.shortDescription LIKE CONCAT('%', :query, '%')")
	List<Post> searchPosts(String query);
	
	Post findByTitle(String title);

}
