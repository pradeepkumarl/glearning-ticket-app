package com.glearning.ticket.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Builder
@AllArgsConstructor
@Data

//JPA Annotations
@Entity
@Table(name = "posts")
public class Post {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(nullable = false)
	private String title;
	@Column(nullable = false)
	private String url;

	@Lob
	@Column(nullable = false)
	private String content;
	@Column(nullable = false)
	private String shortDescription;
	
	@CreationTimestamp
	@Column(nullable = false)
	private LocalDateTime createdOn;

	@UpdateTimestamp
	@Column(nullable = false)
	private LocalDateTime updatedOn;

}
