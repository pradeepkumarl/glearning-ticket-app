package com.glearning.ticket.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import com.glearning.ticket.model.Post;
import com.glearning.ticket.service.PostService;
import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
@RequestMapping("/api/admin/posts")
public class PostRestController {
	
	private final PostService postService;
	
	@GetMapping
	public List<Post> fetchAllPosts() {
		List<Post> posts = this.postService.findAllPost();
		return posts;
	}

}
