package com.glearning.ticket.controller;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.glearning.ticket.model.Post;
import com.glearning.ticket.service.PostService;

import lombok.RequiredArgsConstructor;

@Controller
@RequiredArgsConstructor
@RequestMapping("/admin/posts")
public class PostController {
	
	private final PostService postService;
	
	@GetMapping
	public String fetchAllPosts(Model model) {
		List<Post> posts = this.postService.findAllPost();
		model.addAttribute("posts", posts);
		return "/admin/posts";
	}
	
	@GetMapping("/{postId}/edit")
	public String editPost(@PathVariable("postId") long postId, Model model) {
		Post post = this.postService.findPostById(postId);
		model.addAttribute("post", post);
		return "/admin/edit_post";
	}
	
	@GetMapping("/{postId}/delete")
	public String editPost(@PathVariable("postId") long postId) {
		this.postService.deletePostById(postId);
		return "redirect:/admin/posts";
	}
	
	@PostMapping("/{postId}")
	public String updatePost(
			@PathVariable("postId") long postId,
			@ModelAttribute("post") Post post) {
		
		this.postService.updatePost(postId, post);
		return "redirect:/admin/posts";
	}
	
	@GetMapping("/search")
	public String searchPost(@RequestParam("q") String query, Model model) {
		List<Post> posts = this.postService.searchForPost(query);
		model.addAttribute("posts", posts);
		return "/admin/posts";
	}
	
	 @GetMapping("/{postId}/view")
	    public String viewPost(@PathVariable("postId") long postId,
	                           Model model){
	        Post post = postService.findPostById(postId);
	        model.addAttribute("post", post);
	        return "admin/view_post";

	    }

}
