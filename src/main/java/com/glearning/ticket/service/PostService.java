package com.glearning.ticket.service;

import java.util.List;

import com.glearning.ticket.model.Post;

public interface PostService {
	
	Post createPost(Post post);
	
	Post findPostById(long id);
	
	List<Post> findAllPost();

	Post updatePost(long id, Post updatedPost);
	
	void deletePostById(long id);
	
	Post fetchPostByTitle(String title);
	
	List<Post> searchForPost(String query);

}
