package com.glearning.ticket.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.stereotype.Service;

import com.glearning.ticket.dao.PostRepository;
import com.glearning.ticket.exception.InvalidPostIdException;
import com.glearning.ticket.model.Post;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class PostServiceImpl implements PostService {
	
	private final PostRepository postRepository;

	@Override
	public Post createPost(Post post) {
		Post updatedPost = this.postRepository.save(post);
		return updatedPost;
	}

	@Override
	public Post findPostById(long id) {
		return this.postRepository.findById(id)
				.orElseThrow(() -> new InvalidPostIdException("Invalid post"));
	}

	@Override
	public List<Post> findAllPost() {
		return this.postRepository.findAll();
	}

	@Override
	public Post updatePost(long id, Post updatedPost) {
		Post postFromDB = this.findPostById(id);
		postFromDB.setContent(updatedPost.getContent());
		postFromDB.setShortDescription(updatedPost.getShortDescription());
		return this.postRepository.save(postFromDB);
	}

	@Override
	public void deletePostById(long id) {
		this.postRepository.deleteById(id);
	}

	@Override
	public List<Post> searchForPost(String query) {
		 List<Post> posts = postRepository.searchPosts(query);
	        return posts.stream()
	                .collect(Collectors.toList());
	}

	@Override
	public Post fetchPostByTitle(String title) {
		return this.postRepository.findByTitle(title);
	}

}
